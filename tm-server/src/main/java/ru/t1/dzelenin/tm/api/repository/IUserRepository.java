package ru.t1.dzelenin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

    @Insert("INSERT INTO taskmanager.users (id, login, password, email, first_name, second_name, middle_name, role, locked)"
            + "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{locked})")
    void add(@NotNull UserDTO user);

    @Insert("INSERT INTO taskmanager.users (id, login, password, email, first_name, second_name, middle_name, role, locked)"
            + "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{locked})")
    void addAll(@NotNull Collection<UserDTO> users);

    @Update("UPDATE taskmanager.users SET login = #{login}, password = #{passwordHash}, email = #{email}, first_name = #{firstName},"
            + "second_name = #{lastName}, middle_name = #{middleName}, role = #{role}, locked = #{locked} "
            + "WHERE id = #{id}")
    void update(@NotNull UserDTO user);

    @Override
    @Delete("DELETE FROM taskmanager.users")
    void clear();

    @Override
    @NotNull
    @Select("SELECT id, login, password, email, first_name, second_name, middle_name, role, locked FROM taskmanager.users")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "second_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    List<UserDTO> findAll();

    @Override
    @Select("SELECT count(1) = 1 FROM taskmanager.users WHERE id = #{id}")
    boolean existsById(@Param("id") @NotNull String id);

    @Override
    @Nullable
    @Select("SELECT id, login, password, email, first_name, second_name, middle_name, role, locked FROM taskmanager.users "
            + "WHERE id = #{id}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "second_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findOneById(@Param("id") @NotNull String id);

    @Override
    @Delete("DELETE FROM taskmanager.users WHERE id = #{id}")
    void remove(@NotNull UserDTO user);

    @Override
    @Delete("DELETE FROM taskmanager.users WHERE id = #{id}")
    void removeById(@Param("id") @NotNull String id);

    @Override
    @Select("SELECT count(1) FROM taskmanager.users")
    long getCount();

    @Nullable
    @Select("SELECT id, login, password, email, first_name, second_name, middle_name, role, locked FROM taskmanager.users "
            + "WHERE login = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "second_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findOneByLogin(@Param("login") @NotNull String login);

    @Nullable
    @Select("SELECT id, login, password, email, first_name, second_name, middle_name, role, locked FROM taskmanager.users "
            + "WHERE email = #{email}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "second_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findOneByEmail(@Param("email") @NotNull String email);

}


