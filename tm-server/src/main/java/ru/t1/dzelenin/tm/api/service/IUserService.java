package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.model.UserDTO;
import ru.t1.dzelenin.tm.enumerated.Role;

public interface IUserService extends IService<UserDTO> {

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password);

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @Nullable String email);

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @Nullable Role role);

    void add(@NotNull UserDTO user);

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    void removeByLogin(@NotNull String login);

    void removeByEmail(@NotNull String email);

    @NotNull
    UserDTO setPassword(@NotNull String id, @NotNull String password);

    @NotNull
    UserDTO updateUser(
            @NotNull String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isMailExist(@Nullable String email);

    void lockUserByLogin(@NotNull String login);

    void unlockUserByLogin(@NotNull String login);

}
