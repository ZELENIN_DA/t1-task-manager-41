package ru.t1.dzelenin.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public static Role toRole(@Nullable final String value) {
        if (value == null || value.isEmpty()) return USUAL;
        for (final Role role : values()) {
            if (role.name().equals(value)) return role;
        }
        return USUAL;
    }

}

